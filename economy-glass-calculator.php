<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.managedword.com
 * @since             1.0.0
 * @package           Economy_Glass_Calculator
 *
 * @wordpress-plugin
 * Plugin Name:       Economy Glass Calculator
 * Plugin URI:        https://economyglasscowest.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Christopher Frazier
 * Author URI:        https://www.managedword.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       economy-glass-calculator
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-economy-glass-calculator-activator.php
 */
function activate_economy_glass_calculator() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-economy-glass-calculator-activator.php';
	Economy_Glass_Calculator_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-economy-glass-calculator-deactivator.php
 */
function deactivate_economy_glass_calculator() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-economy-glass-calculator-deactivator.php';
	Economy_Glass_Calculator_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_economy_glass_calculator' );
register_deactivation_hook( __FILE__, 'deactivate_economy_glass_calculator' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-economy-glass-calculator.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_economy_glass_calculator() {

	$plugin = new Economy_Glass_Calculator();
	$plugin->run();

}
run_economy_glass_calculator();
