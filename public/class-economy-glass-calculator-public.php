<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://www.managedword.com
 * @since      1.0.0
 *
 * @package    Economy_Glass_Calculator
 * @subpackage Economy_Glass_Calculator/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Economy_Glass_Calculator
 * @subpackage Economy_Glass_Calculator/public
 * @author     Christopher Frazier <chris.frazier@managedword.com>
 */
class Economy_Glass_Calculator_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		add_action( 'woocommerce_before_calculate_totals', array ($this, 'setMinimum'), 99 );
		add_action( 'woocommerce_before_single_product', array ( $this, 'injectJSON'), 10);

	}

	/**
	 * Get a URL and store it in a transient
	 *
	 * @param	string		$url	URL to get response from.
	 * @return	string				HTTP response body
	 */
	public function getFeed($url) {
		// Check the transient cache for the feed
		if(false === ($value = get_transient($url) ) ){

			// Get a new feed
			$response = wp_remote_get($url);
			$value = $response['body'];

			// Set the transient with the CSV data for 10 minutes
			set_transient($url, $value, 600);
		}
		return $value;
	}

	/**
	 * Parse a CSV string into a PHP array
	 *
	 * @param 	string		$csv	CSV string to parse into PHP array
	 * @return	array				PHP array of parsed CSV contents
	 */
	public function parseCSV($csv) {
		// Parse lines into array
		$lines = explode(PHP_EOL, $csv);

		// Pull the headers for keys
		$header = str_getcsv(array_shift($lines));

		// CSV parse each line
		$array = array();
		foreach ($lines as $line) {

			// Get the values
			$values = str_getcsv($line);

			// Set key -> value pairs
			$value_array = array();

			for ($i=0; $i < count($values); $i++) {
				$value_array[$header[$i]] = $values[$i];
			}

		    $array[] = $value_array;
		}
		return $array;
	}

	/**
	 * Handles injection of JSON data from Google Sheets into product pages
	 *
	 */
	public function injectJSON () {

		$edgework_values = json_encode( $this->parseCSV ( $this->getFeed ( 'https://docs.google.com/spreadsheets/d/1Y9cMEsSZW-7kRGCXBSlDCcr-f4xwL0R9tP2WCT_Ljlc/pub?gid=1886932495&single=true&output=csv' ) ) );
		$fabrication_values = json_encode( $this->parseCSV ( $this->getFeed ( 'https://docs.google.com/spreadsheets/d/1Y9cMEsSZW-7kRGCXBSlDCcr-f4xwL0R9tP2WCT_Ljlc/pub?gid=162583614&single=true&output=csv' ) ) );

		$output = "<script type=\"text/javascript\">\n";
		$output .= "var edgework_values = $edgework_values\n";
		$output .= "var fabrication_values = $fabrication_values\n";
		$output .= "</script>";
		echo $output;
		/*
		Old display code
		global $woocommerce, $post, $product;
		echo "<pre>";
		print_r ( $post );
		var_export ( get_post_meta ( $post->ID, '_wc_price_calculator' ) );
		echo "</pre>";
		*/
	}

	/**
	 * Sets a minimum price for a sheet of glass
	 *
	 * Uses a 3 sq.in. area minimum to calculate a minimum price for the variation selected.
	 *
	 * @param 	object 		$cart	WooCommerce cart object used to handle basic functions
	 */
	public function setMinimum ( $cart ) {

		// Make sure WooCommerce has a cart
		if( !WC()->session->__isset( "reload_checkout" )) {

			// Check each product
			foreach ( $cart->cart_contents as $key => $value ) {

				if ( $value['variation_id'] > 0 ) {

					$product_id = $value['variation_id'];

				} else {

					$product_id = $value['product_id'];

				}

				// Get the price
				$product_variable = wc_get_product( $product_id );

				//$product_variable = new WC_Product_Variable( $product_id );
				$base_price = $product_variable->get_price();

				// Calculate the minimum price
				$minimum_price = $base_price * 0.02;

				// Set the minimum price if needed
				if ( $value['data']->price < $minimum_price ) {
					$value['data']->price = $minimum_price;
				}
			}
		}
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Economy_Glass_Calculator_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Economy_Glass_Calculator_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/economy-glass-calculator-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Economy_Glass_Calculator_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Economy_Glass_Calculator_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( 'wp-util' );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/economy-glass-calculator-public.js', array( 'jquery' ), $this->version, false );

	}

}
