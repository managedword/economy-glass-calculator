(function( $ ) {
	'use strict'

	$( window ).load(function() {

		if ($('#price_calculator').length) {

			var updateOptions = function () {
				width = fractionToDecimal($width_needed.val())
				length = fractionToDecimal($length_needed.val())
				strength = $pa_strength.find('option:selected').text()
				thickness = $pa_thickness.find('option:selected').text()

				// Get the available edgework options
				var edgework_data = _.where(edgework_values, { Thickness: thickness })

				$edgeworks.each(function () {
					var $edgework = $(this)
					// Grab the text of the current edgework option
					var selected = $edgework.find('option:selected').text()

					// Empty the current edgework options and set clean
					$edgework
						.empty()
						.append($('<option>').attr('price', '$0').val('Swiped|$0').html('Swiped'))

					// Refill the options with updated pricing
					for (var i = 0; i < edgework_data.length; i++) {
						var price = edgework_data[i].PricePerInch
						var name = edgework_data[i].Name
						// Based on the type of edge, set the correct pricing
						if ($edgework.parents('.edgework.full').length) {
							var all_edges = gformFormatMoney(price * ((width * 2) + (length * 2)))
							$('<option>').attr('price', all_edges).val(name + '|' + all_edges).html(name).appendTo($edgework)
						} else if ($edgework.parents('.edgework.side_a').length || $edgework.parents('.edgework.side_c').length) {
							var width_edge = gformFormatMoney(price * width)
							$('<option>').attr('price', width_edge).val(name + '|' + width_edge).html(name).appendTo($edgework)
						} else if ($edgework.parents('.edgework.side_b').length || $edgework.parents('.edgework.side_d').length) {
							var length_edge = gformFormatMoney(price * length)
							$('<option>').attr('price', length_edge).val(name + '|' + length_edge).html(name).appendTo($edgework)
						}
					}
				})

				// Get the set of fabrications for the thickness
				var fabrication_data = _.where(fabrication_values, { Thickness: thickness })

				$fabrications.each(function () {
					var $fabrication = $(this)
					var selected = $fabrication.find('option:selected').text()
					$fabrication
						.empty()
						.append($('<option>').attr('price', '').val('').html('None'))
					// Add all options
					for (var i = 0; i < fabrication_data.length; i++) {
						var price = gformFormatMoney(fabrication_data[i].Price)
						var name = fabrication_data[i].Name
						$fabrication.append($('<option>').attr('price', price).val(name + '|' + price).html(name))
					}
				})


				gformInitPriceFields()
			}

			var calculatePrice = function () {
				console.log("Calculating Price")
			}

			var fractionToDecimal = function (val) {
				var decimal = 0,
					parts = val.split(' ')

				for (var i = 0; i < parts.length; i++) {
					var fraction_value = 0
					if (parts[i].indexOf('/') !== -1) {
						var fraction = parts[i].split('/')
						fraction_value = parseFloat(fraction[0]) / parseFloat(fraction[1])
					} else {
						fraction_value = parseFloat(parts[i])
					}
					if (!isNaN(fraction_value)) {
						decimal = decimal + fraction_value
					}
				}
				return decimal
			}

			// Hook up jQuery objects
			var $pa_strength = $('#pa_strength'),
				$pa_thickness = $('#pa_thickness'),
				$length_needed = $('#length_needed'),
				$width_needed = $('#width_needed'),

				// Edgework select elements
				$edgeworks = $('.edgework select'),
				$edgework_full = $('.edgework.full select'),
				$edgework_side_a = $('.edgework.side_a select'),
				$edgework_side_b = $('.edgework.side_b select'),
				$edgework_side_c = $('.edgework.side_c select'),
				$edgework_side_d = $('.edgework.side_d select'),

				// Fabrication select elements
				$fabrications = $('.fabrication select'),
				width = 0,
				length = 0,
				strength = '',
				thickness = ''

			// Bind updateOptions
			$pa_strength.bind('change', updateOptions)
			$pa_thickness.bind('change', updateOptions)
			$length_needed.bind('keyup', updateOptions)
			$width_needed.bind('keyup', updateOptions)

			// Move the price calculator to the correct location
			$('#price_calculator').insertAfter('.variations')

			updateOptions()

			// Set minimums
			/*
			$('#length_needed').attr({ type: 'number', min: '1', max: '96', step: '.001' }).addClass('input-text');
			$('#width_needed').attr({ type: 'number', min: '1', max: '130', step: '.001' }).addClass('input-text');
			*/

		}

	})

})( jQuery );
